import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import KeenUI from 'keen-ui';
import 'keen-ui/dist/keen-ui.css';

import 'vue-tel-input/dist/vue-tel-input.css';

import VeeValidate from 'vee-validate';
import axios from 'axios'
import moment from 'moment'
import Vuesax from 'vuesax'



Vue.config.productionTip = false

import './filter';


import 'vuesax/dist/vuesax.css' //Vuesax styles
Vue.use(Vuesax)

Vue.use(KeenUI);
Vue.use(VeeValidate);
window.axios = axios;
// axios.defaults.baseURL = 'https://api.rubiesbank.io/dev';
axios.defaults.baseURL = 'https://api.rubiesbank.io/prod';
// Vue.use(moment);

// Add a response interceptor
axios.interceptors.response.use(function (response) {
  // Do something with response data
  console.log('Response ===>>', parseInt(response.data.responsecode) > 1000);
  // console.log('Original Response===>>',response);
  if(response.status === 403 || response.status === 401){
    console.log('403 Error!!!')
    store.commit('updateNetworkErrorState', true);
  }
  if(parseInt(response.data.responsecode) > 1000){
    store.commit('updateNetworkErrorState', true);

  }

  return response;
}, function (error) {
  // Do something with response error
  console.log('----Error ===>> session error', error);

  // if (error.toString().includes('timeout')){
  //     console.log('Network Error please handle properly lumbex')
  // }
  //
  store.commit('updateNetworkErrorState', true);


  return Promise.reject(error);
});


// Vue.filter('formatDate', function(value) {
//   if (value) {
//     let diff = moment(value).diff(moment(), 'milliseconds');
//     let duration = moment.duration(diff);
//
//     if (duration.days() === 0){
//       return 'Today'
//     }else if (duration.days() === -1){
//       return 'Yesterday'
//     } else {
//       return moment(String(value)).format('Do LL')
//     }
//     // return moment(String(value)).format('LL')
//     //
//     // return moment(Date(value).subtract(1, 'days').calendar());
//   }
// });

export const enrollmentEventBus = new  Vue();



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
