import Vue from 'vue'
import Vuex from 'vuex'
import * as types from './mutation-types'

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    NetworkErrorState: false,
    currentComponent: 'login',
    enrollComponent: 'stageone',
    username: '',
    appLoading: false,
    dataOnLogin: {},
    sideBarPage: 'dashboard',
    insideCurrentPage: 'dashboard',
    fullBankList: '',
    ConfirmationPageBody: {},
    isTransferActive: false,
    mobileDataBiller: {},
    currentAccountForAction: '',
    IBankerList: [],
    moneyRequestSentList: [],
    moneyRequestReceivedList: [],
    currentAirtimePhoneNo: [],
    currentTransferPhoneNo: [],
    currentAccountToFund: '',
    mobileSideNavActive: false,
    sidebarOpen: false,
    menuIsActive: false,
  },
  getters:{
    getNetworkErrorState: state => {
      return state.NetworkErrorState
    },
    getCurrentComponent: state => {
      return state.currentComponent
    },
    getEnrollComponent: state => {
      return state.enrollComponent
    },
    getUsername: state => {
      return state.username
    },
    getAppLoading: state => {
      return state.appLoading
    },
    startAppLoading: state => {
      return state.appLoading = true
    },
    stopAppLoading: state => {
      return state.appLoading =false
    },
    getDataOnLogin: state => {
      return state.dataOnLogin
    },
    getSideBarPage: state => {
      return state.sideBarPage
    },
    getInsideCurrentPage: state => {
      return state.insideCurrentPage
    },
    getFullBankList: state => {
      return state.fullBankList
    },
    getConfirmationPageBody: state => {
      return state.ConfirmationPageBody
    },
    getIsTransferActive: state => {
      return state.isTransferActive
    },
    getMobileDataBiller: state => {
      return state.mobileDataBiller
    },
    getCurrentAccountForAction: state => {
      return state.currentAccountForAction
    },
    getIBankerList: state => {
      return state.IBankerList
    },
    getMoneyRequestSentList: state => {
      return state.moneyRequestSentList
    },
    getMoneyRequestReceivedList: state => {
      return state.moneyRequestReceivedList
    },
    getCurrentAirtimePhoneNo: state => {
      return state.currentAirtimePhoneNo
    },
    getCurrentTransferPhoneNo: state => {
      return state.currentTransferPhoneNo
    },
    getCurrentAccountToFund: state => {
      return state.currentAccountToFund
    },
    getMobileSideNavActive: state => {
      return state.mobileSideNavActive
    },
    sidebarOpen: state => state.sidebarOpen,
    getMenuIsActive: state => {
      return state.menuIsActive
    },
  },
  mutations: {
    updateNetworkErrorState: (state, payload)=>{
      state.NetworkErrorState = payload
    },
    updateCurrentComponent: (state, payload)=>{
      state.currentComponent = payload
    },
    updateEnrollComponent: (state, payload)=>{
      state.enrollComponent = payload
    },
    updateUsername: (state, payload)=>{
      state.username = payload
    },
    updateAppLoading: (state, payload)=>{
      state.appLoading = payload
    },
    updateDataOnLogin: (state, payload)=>{
      state.dataOnLogin = payload
    },
    updateDataOnLoginAccounts: (state, payload)=>{
      state.dataOnLogin.accountlist = payload
    },
    updateDataOnLoginTransactions: (state, payload)=>{
      state.dataOnLogin.transactionlist = payload
    },
    updateSideBarPage: (state, payload)=>{
      state.sideBarPage = payload
    },
    updateInsideCurrentPage: (state, payload)=>{
      state.insideCurrentPage = payload
    },
    updateFullBankList: (state, payload)=>{
      state.fullBankList = payload
    },
    updateConfirmationPageBody: (state, payload)=>{
      state.ConfirmationPageBody = payload
    },
    updateIsTransferActive: (state, payload)=>{
      state.isTransferActive = payload
    },
    updateMobileDataBiller: (state, payload)=>{
      state.mobileDataBiller = payload
    },
    updateCurrentAccountForAction: (state, payload)=>{
      state.currentAccountForAction = payload
    },
    updateIBankerList: (state, payload)=>{
      state.IBankerList = payload
    },
    updateMoneyRequestSentList: (state, payload)=>{
      state.moneyRequestSentList = payload
    },
    updateMoneyRequestReceivedList: (state, payload)=>{
      state.moneyRequestReceivedList = payload
    },
    updateCurrentAirtimePhoneNo: (state, payload)=>{
      state.currentAirtimePhoneNo = payload
    },
    updateCurrentTransferPhoneNo: (state, payload)=>{
      state.currentTransferPhoneNo = payload
    },
    updateCurrentAccountToFund: (state, payload)=>{
      state.currentAccountToFund = payload
    },
    updateMobileSideNavActive: (state, payload)=>{
      state.mobileSideNavActive = payload
    },
    [types.TOGGLE_SIDEBAR] (state) {
      state.sidebarOpen = !state.sidebarOpen
    },
    updateMenuIsActive: (state, payload)=>{
      state.menuIsActive = payload
    },
  },
  actions: {
    updateNetworkErrorState: ({ commit }, payload)=>{
      commit('updateNetworkErrorState', payload)
    },
    updateCurrentComponent: ({ commit }, payload)=>{
      commit('updateCurrentComponent', payload)
    },
    updateEnrollComponent: ({ commit }, payload)=>{
      commit('updateEnrollComponent', payload)
    },
    updateUsername: ({ commit }, payload)=>{
      commit('updateUsername', payload)
    },
    updateAppLoading: ({ commit }, payload)=>{
      commit('updateAppLoading', payload)
    },
    updateDataOnLogin: ({ commit }, payload)=>{
      commit('updateDataOnLogin', payload)
    },
    updateDataOnLoginAccounts: ({ commit }, payload)=>{
      commit('updateDataOnLoginAccounts', payload)
    },
    updateDataOnLoginTransactions: ({ commit }, payload)=>{
      commit('updateDataOnLoginTransactions', payload)
    },
    updateSideBarPage: ({ commit }, payload)=>{
      commit('updateSideBarPage', payload)
    },
    updateInsideCurrentPage: ({ commit }, payload)=>{
      commit('updateInsideCurrentPage', payload)
    },
    updateFullBankList: ({ commit }, payload)=>{
      commit('updateFullBankList', payload)
    },
    updateConfirmationPageBody: ({ commit }, payload)=>{
      commit('updateConfirmationPageBody', payload)
    },
    updateIsTransferActive: ({ commit }, payload)=>{
      commit('updateIsTransferActive', payload)
    },
    updateMobileDataBiller: ({ commit }, payload)=>{
      commit('updateMobileDataBiller', payload)
    },
    updateCurrentAccountForAction: ({ commit }, payload)=>{
      commit('updateCurrentAccountForAction', payload)
    },
    updateIBankerList: ({ commit }, payload)=>{
      commit('updateIBankerList', payload)
    },
    updateMoneyRequestSentList: ({ commit }, payload)=>{
      commit('updateMoneyRequestSentList', payload)
    },
    updateMoneyRequestReceivedList: ({ commit }, payload)=>{
      commit('updateMoneyRequestReceivedList', payload)
    },
    updateCurrentAirtimePhoneNo: ({ commit }, payload)=>{
      commit('updateCurrentAirtimePhoneNo', payload)
    },
    updateCurrentTransferPhoneNo: ({ commit }, payload)=>{
      commit('updateCurrentTransferPhoneNo', payload)
    },
    updateCurrentAccountToFund: ({ commit }, payload)=>{
      commit('updateCurrentAccountToFund', payload)
    },
    updateMobileSideNavActive: ({ commit }, payload)=>{
      commit('updateMobileSideNavActive', payload)
    },
    toggleSidebar ({ commit, state }) {
      commit(types.TOGGLE_SIDEBAR)
    },
    updateMenuIsActive: ({ commit }, payload)=>{
      commit('updateMenuIsActive', payload)
    },
  }
})
