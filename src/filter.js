import Vue from 'vue'
import moment from 'moment'


// create a new axios instance

Vue.filter('moneyFormat', value => {
    if (!value) return '';
    return value.toLocaleString()
});

Vue.filter('formatDateRelatively', value => {
    if (value) {
        let diff = moment(value).diff(moment(), 'milliseconds');
        let duration = moment.duration(diff);

        if (duration.days() === 0 && duration.months() === 0) {
            return 'Today'
        } else if (duration.days() === -1 && duration.months() === 0) {
            return 'Yesterday'
        } else {
            // return moment(String(value)).format('Do LL')
            return moment(String(value)).format('DD-MMM-YYYY')
        }
    }
});

Vue.filter('newAmountFormatter', value => {
    value = value.replace(/,/g, '');


    let outgoingAmount = (parseFloat(value).toFixed(2)).toString();
    let indexOfThePoint = outgoingAmount.indexOf('.');

    String.prototype.insert = function (index, string) {
        if (index > 0)
            return this.substring(0, index) + string + this.substring(index, this.length);
        return string + this;
    };
    let answer = outgoingAmount;
    let count = 0;
    for (let i = indexOfThePoint; i >= 1; i--){
        if(count === 3){
            count = 1;
            answer = answer.insert(i, ",")
        }
        else {
            count++
        }
    }
    return answer;
});

Vue.filter('firstNameFromFullName', value => {
    if (!value) return '';
    value = value.toString();
    return value.split(' ').slice(0, -1).join(' ').toLowerCase()
});

Vue.filter('toUppercase', value => {
    if (!value) return '';
    value = value.toUpperCase();
    return value
});

Vue.filter('toLowercase', value => {
    if (!value) return '';
    value = value.toLowerCase();
    return value
});

Vue.filter('firstCaseCapital', value => {
    if (!value) return '';
    value = value.charAt(0).toUpperCase() + value.slice(1);
    return value
});
Vue.filter('firstCaseCapitalSpace', value => {
    if (!value) return '';
    value =value.toLowerCase();
    if (!value.includes(' ')){
        value = value.charAt(0).toUpperCase() + value.slice(1);
    } else {
        value = titleCase(value);

        function titleCase(str) {
            let splitStr = str.toLowerCase().split(' ');
            for (let i = 0; i < splitStr.length; i++) {
                // You do not need to check if i is larger than splitStr length, as your for does that for you
                // Assign it back to the array
                splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
            }
            // Directly return the joined string
            return splitStr.join(' ');
        }

    }
    return value
});

Vue.filter('moneyToDecimal', value => {
    if (!value) return '';
    // value = value.charAt(0).toUpperCase() + value.slice(1);
    value = parseFloat(value).toFixed(2);
    return value
});


// export default instance