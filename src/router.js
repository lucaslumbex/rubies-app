import Vue from 'vue'
import Router from 'vue-router'

import Base from './components/Base.vue'
import WorkSpace from './components/utility/WorkSpace.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Base',
            component: Base
        },
        {
            path: '/workspace',
            name: 'WorkSpace',
            component: WorkSpace
        }
    ]
})
